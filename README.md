![coverage](https://gitlab.com/guillaumeduveau/docker-php7/badges/master/build.svg)

This PHP image :

+ is based on Alpine
+ runs PHP 7 in FPM mode, for coupling with an Apache / NGINX container
+ runs PHP-FPM with UID=1000 and GID=1000 (which is generally corresponding to the user/group of your own user in Ubuntu, so you don't have any permission problems)
+ has the usual PHP extensions for Drupal (GD, ...) and xDebug
+ is for dev purposes, NOT PRODUCTION
